<?php

class InputFurniture extends ValidateForm implements InsertData
{
    protected static $fields = ['height', 'width', 'lenght'];
    public $type = 'Dimensions:';

    public function insertData()
    {
        if ($this->validateFrom() && $this->fieldValidation()) {

            $height = $this->data['height'];
            $width = $this->data['width'];
            $lenght = $this->data['lenght'];

            $sql = "INSERT INTO producttable (sku, name, price, type, value)
            VALUES ('$this->sku', '$this->name', '$this->price $', '$this->type' ,'$height x $width x $lenght')";

            if ($this->connectDB()->query($sql) === TRUE) {
                $this->reDirect();
            } else {
                $this->reDirect();
            }
              
        } else {
            $this->reDirect();
        }
    }

    private function fieldValidation()
    {
        if ($this->validateHeight() && $this->validateWidth() && $this->validateLenght()) {
            return true;
        }
    }

    private function validateHeight()
    {
        $val = trim($this->data['height']);
        if (!empty($val) && preg_match(self::NUM_PATTERN, $val)) {
            return true;
        }
    }

    private function validateWidth()
    {
        $val = trim($this->data['width']);
        if (!empty($val) && preg_match(self::NUM_PATTERN, $val)) {
            return true;
        }
    }

    private function validateLenght()
    {
        $val = trim($this->data['lenght']);
        if (!empty($val) && preg_match(self::NUM_PATTERN, $val)) {
            return true;
        }
    }
}
?>