<?php

class InputBook extends ValidateForm implements InsertData
{
    protected static $fields = ['weight'];
    public $type = 'Weight:';

    public function insertData()
    {
        if ($this->validateFrom() && $this->validateWeight()) {
            $weight = $this->data['weight'];
            $sql = "INSERT INTO producttable (sku, name, price, type, value)
            VALUES ('$this->sku', '$this->name', '$this->price $', '$this->type' ,'$weight KG')";

            if ($this->connectDB()->query($sql) === TRUE) {
                $this->reDirect();
            } else {
                $this->reDirect();
            }
              
        } else {
            $this->reDirect();
        }
    }

    private function validateWeight()
    {
        $val = trim($this->data['weight']);

        if (!empty($val) && preg_match(self::NUM_PATTERN, $val)) {
            return true;
        }
    }
}
?>