<?php

class DisplayProducts extends ConnectDB
{

    public $data = array();

    public function __construct()
    {
        $sql = "SELECT * FROM producttable";
        $result = $this->connectDB()->query($sql);
        
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
              $this->data[] = $row;
            }
          } else {
            echo "0 results";
        }
        
        $this->connectDB()->close();
    }

    public function displayId($x)
    {
        return $this->data[$x]["id"];
    }

    public function displaySku($x)
    {
        return $this->data[$x]["sku"];
    }

    public function displayName($x)
    {
        return $this->data[$x]["name"];
    }

    public function displayPrice($x)
    {
        return $this->data[$x]["price"];
    }

    public function displayType($x)
    {
        return $this->data[$x]["type"];
    }

    public function displayValue($x)
    {
        return $this->data[$x]["value"];
    }

}

?>