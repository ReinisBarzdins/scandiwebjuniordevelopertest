<?php

class InputDVD extends ValidateForm implements InsertData
{
    protected static $fields = ['size'];
    public $type = 'Size:';

    public function insertData()
    {
        if ($this->validateFrom() && $this->validateSize()) {
            $size = $this->data['size'];
            $sql = "INSERT INTO producttable (sku, name, price, type, value)
            VALUES ('$this->sku', '$this->name', '$this->price $', '$this->type' ,'$size MB')";

            if ($this->connectDB()->query($sql) === TRUE) {
                $this->reDirect();
            } else {
                $this->reDirect();
            }

        } else {
            $this->reDirect();
        }
    }

    private function validateSize()
    {
        $val = trim($this->data['size']);
        if (!empty($val) && preg_match(self::NUM_PATTERN, $val)) {
            return true;
        }
    }
}
?>