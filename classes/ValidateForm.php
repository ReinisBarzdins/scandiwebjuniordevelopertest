<?php

interface InsertData 
{
    public function insertData();
}

class ValidateForm extends ConnectDB
{
    const SKU_NAME_PATTERN = '/^[\sA-Za-z0-9_-]+$/';
    const PRICE_PATTERN = '/^\d+[.]+\d{2}$/';
    const NUM_PATTERN = '/^\d*$/';
    protected static $fields = ['sku','name','price'];
    protected $data;
    protected $sku;
    protected $name;
    protected $price;
    

    public function __construct($post_data)
    {
        $this->data = $post_data;
    }

    protected function reDirect()
    {
        header("Location: /");
        die();
    }

    protected function validateFrom()
    {
        foreach(self::$fields as $field) {
            if (!array_key_exists($field, $this->data)) {
                trigger_error("$field is not present in data");
                return;
            }
        }

        if ($this->validateSku() && $this->validateName() && $this->validatePrice()) {
            $this->sku = $this->data['sku'];
            $this->name = $this->data['name'];
            $this->price = $this->data['price'];
            return true;
        }
    }

    private function validateSku()
    {
        $val = trim($this->data['sku']);
        if (!empty($val) && preg_match(self::SKU_NAME_PATTERN, $val)) {
            return true;
        }
    }

    private function validateName()
    {
        $val = trim($this->data['name']);
        if (!empty($val) && preg_match(self::SKU_NAME_PATTERN, $val)) {
            return true;
        }
    }
    
    private function validatePrice()
    {
        $val = trim($this->data['price']);
        if (!empty($val) && preg_match(self::PRICE_PATTERN, $val)) {
            return true;
        }
    }
}
?>