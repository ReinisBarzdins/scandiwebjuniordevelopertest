<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="css/add-product-css.css">
  </head>
  <body>
    <div class="content">
      <div class="header">
        <div class="headline">
          <h1>Product Add</h1>
        </div>
        <div class="buttons">
          <button class="save" type="submit" name="save" form="product_form" disabled>Save</button>
          <a href="/"><button class="cancel" type="button" name="cancel">Cancel</button></a>
        </div>
      </div>

      <hr class="line">


      <div class="form">
        <form id="product_form" action="" method="post" >
          <label for="sku">SKU</label>
          <input id="sku" type="text" name="sku" value=""><br>
          <label for="name">Name</label>
          <input id="name" type="text" name="name" value=""><br>
          <label for="price">Price($)</label>
          <input id="price" type="text" name="price" value=""><br>
          <label for="productType">Type switcher</label>
          <select id="productType" name="type" onchange="selected()">
            <option selected="true" hidden disabled="disabled"></option>
            <option value="DVD">DVD</option>
            <option value="Book">Book</option>
            <option value="Furniture">Furniture</option>
          </select><br>
          <div id="dynamicForm"></div>
        </form>
        <?php
        
        if (isset($_POST['save'])) {
            require_once ('includes/autoloader.inc.php');
            $dynamicClassName = 'Input'.$_POST['type'];
            $insert = new $dynamicClassName($_POST);
            $insert->insertData();
        }
        ?>
        <span id="error"></span>
      </div>
      <hr class="line2">
      <h3 class="footer-text">Scandiweb Test assignment</h3>
    </div>
  </body>
  <script src="js/add-product-js.js"></script>
</html>
