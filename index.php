<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="css/index-css.css">
  </head>
  <body>
    <div class="content">
      <div class="header">
        <div class="headline">
          <h1>Product List</h1>
        </div>
        <div class="buttons">
          <a href="add-product"><button id="add" type="button" name="add">ADD</button></a>
          <button id="delete-product-btn" type="submit" name="delete" form="product_list">MASS DELETE</button>
        </div>
      </div>

      <hr class="line">

      <div class="form">
        <form id="product_list" method="post">
        <?php
        require_once ('includes/autoloader.inc.php');
        $display = new DisplayProducts();
        
        if (isset($_POST['delete'])) {
            $delete = new DeleteProducts($_POST);
            $delete->delete();

        }


        for ($x = 0; $x < count($display->data) ; $x++) {
            $id = $display->displayId($x);
            $sku = $display->displaySku($x);
            $name = $display->displayName($x);
            $price = $display->displayPrice($x);
            $type = $display->displayType($x);
            $value = $display->displayValue($x);

            echo "
            <div class='item'>
            <input class='delete-checkbox' type='checkbox' name='checkbox[]' value='$id'><br>
            <h4 class='sku' name='sku' value='test'>
            ";
            echo htmlentities($sku, ENT_QUOTES, 'UTF-8');
            echo "
            </h4>
            <h4 class='name'>
            ";
            echo htmlentities($name, ENT_QUOTES, 'UTF-8');
            echo "
            </h4>
            <h4 class='price'>
            ";
            echo htmlentities($price, ENT_QUOTES, 'UTF-8');
            echo "
            </h4>
            <h4 class='value'>
            ";
            echo htmlentities($type, ENT_QUOTES, 'UTF-8')." ".htmlentities($value, ENT_QUOTES, 'UTF-8');
            echo "
            </h4>
            </div>
            ";
        }
        ?>
        </form>
      </div>
      <hr class="line2">
      <h3 class="footer-text">Scandiweb Test assignment</h3>
    </div>

  </body>
</html>
