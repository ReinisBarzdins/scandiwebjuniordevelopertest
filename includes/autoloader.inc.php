<?php
spl_autoload_register('myAutoLoader');

function myAutoLoader($className)
{
    $path = "classes/";
    $fullPath = $path . str_replace('\\', '/', $className) . '.php';

    if (!file_exists($fullPath)) {
        return false;
    }
   
    require $fullPath;
}
?>
