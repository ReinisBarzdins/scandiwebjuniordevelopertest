let selectElementValue;
let generateForm;
let validation;
let validationPattern = {
skuPattern : /^[\sA-Za-z0-9_-]+$/,
namePattern : /^[\sA-Za-z0-9_-]+$/,
pricePattern : /^\d+[.]+\d{2}$/,
numberPattern : /^\d*$/
}

class FormDiv{
  constructor() {
    this.div = document.getElementById("dynamicForm");
    while (this.div.firstChild) {
    this.div.firstChild.remove()
    }
  }
}

class FormElements extends FormDiv {
  constructor() {
    super();
    this.label = document.createElement("label");
    this.input = document.createElement("input");
    this.br = document.createElement("br");
    this.span = document.createElement("span");
  }
  generate(){
    this.div.appendChild(this.label);
    this.div.appendChild(this.input);
    this.div.appendChild(this.br);
    this.div.appendChild(this.span);
  }
}

class DVD extends FormElements{
  constructor() {
    super();
    this.label.setAttribute("for","size");
    this.label.innerHTML = "Size (MB)";
    Object.assign(this.input, {
      id: 'size',
      type: 'number',
      name: 'size',
    })
    this.span.innerHTML = "Please, provide size";
    super.generate();
  }
}

class Furniture extends FormElements{
  constructor(span) {
    super(span);
    this.parametersLabel = ["Height (CM)","Width (CM)","Length (CM)"];
    this.parametersInput = ["height","width","length"];
    this.label = [];
    this.input = [];
    for (var i = 0; i < 3; i++) {
      this.label[i] = document.createElement("label");
      this.input[i] = document.createElement("input");
      this.label[i].setAttribute("for",this.parametersInput[i]);
      this.label[i].innerHTML = this.parametersLabel[i];
      Object.assign(this.input[i], {
        id: this.parametersInput[i],
        type: 'number',
        name: this.parametersInput[i],
      })
      this.div.appendChild(this.label[i]);
      this.div.appendChild(this.input[i]);
      this.div.appendChild(this.br.cloneNode(true));
    }
    this.span.innerHTML = "Please, provide dimensions";
    this.div.appendChild(this.span);
  }
}

class Book extends FormElements{
  constructor() {
    super();
    this.label.setAttribute("for","weight");
    this.label.innerHTML = "Weight (KG)";
    Object.assign(this.input, {
      id: 'weight',
      type: 'number',
      name: 'weight',
    })
    this.span.innerHTML = "Please, provide weight";
    super.generate();
  }
}

class Form {
  constructor() {
    this.form = document.getElementById("product_form");
    this.error = document.getElementById("error");
    this.button = document.getElementsByClassName("save");
  }
  fieldsEmpty(){
    return this.error.innerHTML = "Please, submit required data";
  }
  fieldsInvalid(){
    return this.error.innerHTML = "Please, provide the data of indicated type";
  }
  fieldsValid(){
    return this.error.innerHTML = "";
  }
  buttonEnabled(){
    return this.button[0].disabled = false;
  }
  buttonDisabled(){
    return this.button[0].disabled = true;
  }
}

class Validation extends Form{
  constructor(form,error) {
    super(form,error)
  }
  checkEmpty(){
    this.sku = this.form["sku"].value;
    this.name = this.form["name"].value;
    this.price = this.form["price"].value;
    if (this.skuEmpty() || this.nameEmpty() || this.priceEmpty()) {
      return true;
    }
  }
  checkValidation(){
    if (!this.skuValidation() || !this.nameValidation() || !this.priceValidation()) {
      return true;
  }
}
  skuValidation(){
    return validationPattern.skuPattern.test(this.sku);
  }
  skuEmpty(){
    return this.sku == "";
  }
  nameValidation(){
    return validationPattern.namePattern.test(this.name);
  }
  nameEmpty(){
    return this.name == "";
  }
  priceValidation(){
    return validationPattern.pricePattern.test(this.price);
  }
  priceEmpty(){
    return this.price == "";
  }
}

class DVDValidation extends Validation{
  constructor(form,error) {
    super(form,error);
  }
  check(){
    this.dvd = generateForm.input.value;
    if (super.checkEmpty() || this.dvdEmpty()) {
      super.fieldsEmpty();
      super.buttonDisabled();
    } else if(super.checkValidation() || !this.dvdValidation()){
      super.fieldsInvalid();
      super.buttonDisabled();
    } else {
      super.fieldsValid();
      super.buttonEnabled();
    }
  }
  dvdValidation(){
    return validationPattern.numberPattern.test(this.dvd);
  }
  dvdEmpty(){
    return this.dvd == "";
  }
}

class BookValidation extends Validation{
  constructor(form,error) {
    super(form,error);
  }
  check(){
    this.book = generateForm.input.value;
    if (super.checkEmpty() || this.bookEmpty()) {
      super.fieldsEmpty();
      super.buttonDisabled();
    } else if(super.checkValidation() || !this.bookValidation()){
      super.fieldsInvalid();
      super.buttonDisabled();
    } else {
      super.fieldsValid();
      super.buttonEnabled();
    }
  }
  bookValidation(){
    return validationPattern.numberPattern.test(this.book);
  }
  bookEmpty(){
    return this.book == "";
  }
}

class FurnitureValidation extends Validation{
  constructor(form,error) {
    super(form,error);
  }
  check(){
    this.input = [generateForm.input[0].value,
    generateForm.input[1].value,
    generateForm.input[2].value];
    if (super.checkEmpty() || this.furnitureEmpty()) {
      super.fieldsEmpty();
      super.buttonDisabled();
    } else if(super.checkValidation() || !this.furnitureValidation()){
      super.fieldsInvalid();
      super.buttonDisabled();
    } else {
      super.fieldsValid();
      super.buttonEnabled();
    }
  }
  furnitureValidation(){
    return (validationPattern.numberPattern.test(this.input[0]) ||
    validationPattern.numberPattern.test(this.input[1]) ||
    validationPattern.numberPattern.test(this.input[2]));
  }
  furnitureEmpty(){
    return this.input[0] == "" || this.input[1] == "" || this.input[2] == "";
  }
}

function selected(){
 selectElementValue = document.getElementById("productType").value;
  switch (selectElementValue) {
    case "DVD":
      generateForm = new DVD();
      validation = new DVDValidation();
      break;
    case "Furniture":
      generateForm = new Furniture();
      validation = new FurnitureValidation();
      break;
    case "Book":
      generateForm = new Book();
      validation = new BookValidation();
      break;
  }
}

document.getElementById("product_form").onkeyup = checkForm;
function checkForm(){
  if (selectElementValue != undefined) {
    validation.check();
  }

}
